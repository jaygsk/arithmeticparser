﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;


public class ArithmeticParser : MonoBehaviour {

    [SerializeField] Text input;
    [SerializeField] Text result;
    [SerializeField] GameObject particle;
    [SerializeField] GameObject[] muteButton;
    [SerializeField] AudioSource audio;


    enum Token { MultDiv, AddSub, Null };

    private Token token; 
    private bool mute = false;
  

    private void Start()
    {
        //disable particle effect
        particle.SetActive(false);

        //reset token
        token = Token.Null; 
    }

    //break the string into a list
    List<string> StringToList(string exp)
    {
        //handle negative number in first position
        if (exp.Substring(0,1) == "-")
        {
            var temp = exp; 
            exp = "0"+temp;
        }

        List<string> expression = new List<string>();
        string tempExp = "";
        
        //break the string into a list with enclosing character '#'
        tempExp = exp.Replace("/", "#/#").Replace("*", "#*#").Replace("+", "#+#").Replace("-", "#-#");
        expression = tempExp.Split('#').ToList();

        //handle unary operator (negative number) not in first position
        for (int i=0; i < expression.Count; i++)
        {
            //get the empty element in the list between "-" and preceding operator
            if (expression[i] == "")
            {
                //convert the next number into negative, replace the empty element and remove the remaining elements
                var unary = Convert.ToDouble(expression[i + 2]);
                expression[i] = (unary*(-1)).ToString();
                expression.RemoveRange(i + 1, 2);
            }
        }
        return expression;
    }

    //create a basic expression 
    string CreateBasicExpression(string exp)
    {
        string result = "";
        //Add the expression inside parenthesis
        result += "(" + exp.Replace(" ", "") + ")";
        //handle some exception
        result = result.Replace(")(", ")*(");
       
        return result;
    }


    public string Calculate()
    {
        string basicExpression = CreateBasicExpression(input.text);

        while (basicExpression.Contains("("))
        {
            //Get the first expression -> exp
            string exp1 = basicExpression.Substring(basicExpression.LastIndexOf("(") + 1);
            string exp = exp1.Substring(0, exp1.IndexOf(")"));

            //break the string into a list
            List<string> expressionList = StringToList(exp);

            //while the list is greater than 1 convert to double and do the math
            while (expressionList.Count > 1)
            {
                while (expressionList.Contains("*") || expressionList.Contains("/"))
                {
                    ConvertToDouble(expressionList, Token.MultDiv);
                }
                while (expressionList.Contains("+") || expressionList.Contains("-"))
                {
                    ConvertToDouble(expressionList, Token.AddSub);
                }
            }
            //replace basic expression with the resul
            basicExpression = basicExpression.Replace("(" + exp + ")", expressionList[0].ToString());
        }

        return basicExpression;
    }

    public void Result()
    {
        result.text = Calculate();
        particle.SetActive(true);
    }

    //handle the mute button
    public void Mute()
    {
        mute = !mute;

        if (mute)
        {
            muteButton[0].SetActive(true);
            muteButton[1].SetActive(false);
            audio.Stop();
        }
        else
        {
            muteButton[0].SetActive(false);
            muteButton[1].SetActive(true);
            audio.Play();
        }
    }

    //convert the list into double according to the operation
    List<string> ConvertToDouble(List<string> exp, Token t)
    { 
       double doubleVal = 0;
       bool found = false;

       for (int i = 0; i < exp.Count; i++)
       {
            //first do the multiplication and division
            if (t == Token.MultDiv)
            {
                //if the operator is found, convert the precedent and the subsequent expression to double and do the math
                if (exp[i] == "*") { doubleVal = Convert.ToDouble(exp[i - 1]) * Convert.ToDouble(exp[i + 1]); found = true; }
                else if (exp[i] == "/") { doubleVal = Convert.ToDouble(exp[i - 1]) / Convert.ToDouble(exp[i + 1]); found = true; }
            }
            else if (t == Token.AddSub)
            {   
                //if the operator is found, convert the precedent and the subsequent expression to double and do the math
                if (exp[i] == "+") { doubleVal = Convert.ToDouble(exp[i - 1]) + Convert.ToDouble(exp[i + 1]); found = true; }
                else if (exp[i] == "-") { doubleVal = Convert.ToDouble(exp[i - 1]) - Convert.ToDouble(exp[i + 1]); found = true; }
            }  
            
       if (found) //if operator was found, remove the string and replace with the result
           {
              exp.RemoveRange(i - 1, 3);
              exp.Insert(i - 1, doubleVal.ToString());
              //reset the index to reiterate the list
              i = 0;
              found = false;
           }
       }
     
       return exp;
    } 
}
